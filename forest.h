#pragma once

#include <cassert>
#include <unordered_set>
#include <iostream>
#include <fstream>
#include <iomanip>
#define MAX_TOWER_HEIGHT 5
#define PLACEHOLDER_TID 0
#define MAX_KCAS 100

#include "kcas/kcas.h"
#include "util.h"
struct ANode;
struct GNode;
struct LNode;

struct ANode {
    GNode * node;
    LNode * edge;
    casword<ANode *> next;

    ANode(GNode * node, LNode * edge) {
        this->edge = edge;
        this->node = node;
        next.setInitVal((ANode *) NULL);
    }
};

struct GNode {
    int key;
    casword<ANode *> nHead;
    LNode * loop;
    casword<uint64_t> ver;
    
    char padding[PADDING_BYTES]; //false sharing is a real bitch
    
    GNode(int key) {
        this->key = key;
        nHead.setInitVal((ANode *) NULL);
        ver.setInitVal(0);
        loop = NULL;
    }

    LNode * getEdge(GNode * other) {
        ANode * currN = this->nHead;
        while(currN != NULL) {
            if(currN->node == other) return currN->edge;
            currN = currN->next;
        }
        return NULL;
    }
};

struct LNode {
    casword<LNode *> left;
    casword<LNode *> right;
    LNode * up = NULL;
    LNode * down = NULL;
    GNode * from = NULL;
    GNode * to = NULL;
    casword<uint64_t> ver;

    LNode() {
        left.setInitVal((LNode *) NULL);
        right.setInitVal((LNode *) NULL);
        ver.setInitVal(0);
    }

    LNode(LNode * down) { //don't call me concurrently
        down->up = this;
        this->down = down;
        left.setInitVal((LNode *) NULL);
        right.setInitVal((LNode *) NULL);
        ver.setInitVal(0);
    }

    LNode(GNode * from, GNode * to) {
        this->from = from;
        this->to = to;
        left.setInitVal((LNode *) NULL);
        right.setInitVal((LNode *) NULL);
        ver.setInitVal(0);
    }

    LNode(GNode * from, GNode * to, LNode * down) {
        down->up = this;
        this->down = down;
        this->from = from;
        this->to = to;
        left.setInitVal((LNode *) NULL);
        right.setInitVal((LNode *) NULL);
        ver.setInitVal(0);
    }
};

using namespace std;

class Forest {
private:
    volatile char padding0[PADDING_BYTES];
    const int numThreads;


public:
    volatile char padding1[PADDING_BYTES];
    PaddedRandom rngs[MAX_THREADS];
    volatile char padding2[PADDING_BYTES];
    Forest(const int _numThreads);
    ~Forest();
    void addRoot(GNode * newRoot);
    void graph(GNode * root, int arg, int arg2);
    void printDebugging(GNode * gNode);
    bool cut(const int tid, GNode * v, GNode * w);
    bool link(const int tid, GNode * v, GNode * w);
    bool connected(const int tid, GNode * v, GNode * w);

    void createSentinelTower(const int tid, LNode ** tower);
    void createTower(const int tid, GNode * from, GNode * to, LNode ** tower);

    GNode * getRoot(GNode * node);
    GNode * getRoot(LNode * node);

    LNode * getMin(GNode * node);
    LNode * getMax(GNode * node);

private:
    void generateList(const int tid, GNode * root);
    int getRandomLevel(const int tid);
    bool outputGraph(GNode * gNode, ofstream &dot, int left, int down, std::unordered_set<GNode *> &gNodes);
    void outputList(LNode * lNode, ofstream &dot, int up, int left, std::unordered_set<LNode *> &lNodes);
    bool areNeighbours(GNode * u, GNode * v);
    void printTowers(LNode ** t);

    void getTower(LNode * start, LNode ** tower, bool forward);
    bool getInternalTowers(LNode * start, LNode * end, LNode ** left, LNode ** right);
    void getSentinel(LNode * node, LNode ** tower);
    void nullTower(LNode ** tower);
    LNode * getFirstNonNull(int height, LNode ** tower);

    template<typename... Args>
    LNode * getFirstNonNull(int height, LNode ** tower, Args... args);


};

Forest::Forest(const int _numThreads)
: numThreads(_numThreads) {
    for(int i = 0; i < MAX_THREADS; i++) {
        rngs[i].setSeed(i + 5); //mess with seed to get different starting vals
    }
}

Forest::~Forest() {
}

/*
 * I DO NOT EXPECT THIS TO BE CALLED CONCURRENTLY. 
 * Used to initialize the forest before calling other functions. 
 */
void Forest::addRoot(GNode * newR) {
    generateList(PLACEHOLDER_TID, newR);
}

void Forest::generateList(const int tid, GNode * node) {
    LNode * prev[MAX_TOWER_HEIGHT];
    LNode * curr[MAX_TOWER_HEIGHT];
    LNode * next[MAX_TOWER_HEIGHT];

    createSentinelTower(tid, prev);
    createTower(tid, node, node, curr);
    createSentinelTower(tid, next);

    for(int i = 0; i < MAX_TOWER_HEIGHT; i++) {
        if(curr[i] != NULL) {
            curr[i]->left.setInitVal(prev[i]);
            prev[i]->right.setInitVal(curr[i]);
            curr[i]->right.setInitVal(next[i]);
            next[i]->left.setInitVal(curr[i]);
        }
        else {
            prev[i]->right.setInitVal(next[i]);
            next[i]->left.setInitVal(prev[i]);
        }
    }
    node->loop = curr[0];
    assert(node->loop->from == node && node->loop->from == node);
}

void Forest::createTower(const int tid, GNode * from, GNode * to, LNode ** tower) {
    int height = getRandomLevel(tid);
    tower[0] = new LNode(from, to);
    for(int i = 1; i < height; i++) tower[i] = new LNode(from, to, tower[i - 1]);
    for(int i = height; i < MAX_TOWER_HEIGHT; i++) tower[i] = NULL;
}

void Forest::createSentinelTower(const int tid, LNode ** tower) {
    tower[0] = new LNode();
    for(int i = 1; i < MAX_TOWER_HEIGHT; i++) tower[i] = new LNode(tower[i - 1]);
}

int Forest::getRandomLevel(const int tid) {
    int i, level = 1;
    for(i = 0; i < MAX_TOWER_HEIGHT - 1; i++) {
        if((rngs[tid].nextNatural() % 100) < 50) level++;
        else break;
    }
    return level;
}



//concurrent

bool Forest::cut(const int tid, GNode * v, GNode * w) {
//    cout << "CUTTING**************" << endl;

    while(true) {
        kcas::start();
        uint64_t vVer = v->ver;
        uint64_t wVer = w->ver;
        kcas::add(
                &v->ver, vVer, vVer + 1,
                &w->ver, wVer, wVer + 1);

        LNode * vw = v->getEdge(w);
        LNode * wv = w->getEdge(v);


        //assert((vw == NULL && wv == NULL) || (vw != NULL && wv != NULL)); //seq assert
        if(vw == NULL || wv == NULL) return false; //not connected??
        assert(wv->from == w && wv->to == v);
        assert(vw->from == v && vw->to == w);

        auto vRoot = getRoot(vw);
        auto wRoot = getRoot(wv);

        if(wRoot != w && wRoot != v) { //if not same we should have failed already
            uint64_t wRootVer = wRoot->ver;
            kcas::add(&wRoot->ver, wRootVer, wRootVer + 1); //synchro on root
        }

        LNode * min = getMin(v); //remove redundant call here

        LNode * max = getMax(v); //remove redundant call here


        LNode * vwRight = vw->right;
        LNode * wvLeft = wv->left;

        LNode * A[MAX_TOWER_HEIGHT];
        LNode * B[MAX_TOWER_HEIGHT];
        LNode * C[MAX_TOWER_HEIGHT];
        LNode * D[MAX_TOWER_HEIGHT];
        LNode * E[MAX_TOWER_HEIGHT];
        LNode * F[MAX_TOWER_HEIGHT];

        if(!getInternalTowers(vwRight, wvLeft, C, D)) {
            LNode * temp = vw;
            vw = wv;
            wv = temp;

            GNode * t = v;
            v = w;
            w = t;
            vwRight = vw->right;
            wvLeft = wv->left;
            assert(getInternalTowers(vwRight, wvLeft, C, D));
        }

        LNode * wvRight = wv->right;
        LNode * vwLeft = vw->left;

        LNode * S1[MAX_TOWER_HEIGHT];
        getSentinel(min, S1);
        LNode * S2[MAX_TOWER_HEIGHT];
        getSentinel(max, S2);
        LNode * S3[MAX_TOWER_HEIGHT];
        createSentinelTower(tid, S3);
        LNode * S4[MAX_TOWER_HEIGHT];
        createSentinelTower(tid, S4);
//        cout << "2" << endl;

        if(vwLeft != min) {
//            cout << "vwLeft != min" << endl;

            getInternalTowers(min->right, vwLeft, A, B); //Get L1 Right
        }
        else {
            nullTower(A);
            nullTower(B);
        }
//        cout << "3" << endl;

        if(wvRight != max) {
//            cout << "wvLeft != max" << endl;
            getInternalTowers(wvRight, max->left, E, F); //Get L3 Left
        }
        else {
            nullTower(E);
            nullTower(F);
        }
//        cout << "4" << endl;
//
//
//        cout << "5" << endl;
//        cout << "S1: ";
//        printTowers(S1);
//        cout << "S2: ";
//        printTowers(S2);
//
//        cout << "A: ";
//        printTowers(A);
//        cout << "B: ";
//        printTowers(B);
//        cout << "C: ";
//        printTowers(C);
//        cout << "D: ";
//        printTowers(D);
//        cout << "E: ";
//        printTowers(E);
//        cout << "F: ";
//        printTowers(F);



        for(int i = 0; i < MAX_TOWER_HEIGHT; i++) {
            LNode * old = S1[i]->right;
            kcas::add(&S1[i]->right, old, getFirstNonNull(i, A, E, S2));

            if(B[i] != NULL) {
                old = B[i]->right; //should be B[i]
                kcas::add(&B[i]->right, old, getFirstNonNull(i, E, S2));
            }

            if(C[i] != NULL) {
                old = C[i]->left;
                kcas::add(&C[i]->left, old, S3[i]);
                S3[i]->right.setInitVal(C[i]);
            }
            else {
                S3[i]->right.setInitVal(S4[i]);
            }

            if(D[i] != NULL) {
                old = D[i]->right;
                kcas::add(&D[i]->right, old, S4[i]);
                S4[i]->left.setInitVal(D[i]);
            }
            else {
                S4[i]->left.setInitVal(S3[i]);
            }

            if(E[i] != NULL) {
                old = E[i]->left;
                kcas::add(&E[i]->left, old, getFirstNonNull(i, B, S1));
            }
            old = S2[i]->left;
            kcas::add(&S2[i]->left, old, getFirstNonNull(i, F, B, S1));
        }


        //Graph node removal 
        ANode * prev = NULL;
        ANode * curA = v->nHead;
        bool retry = false;
        while(true) {
            if(curA == NULL) {
                retry = true;
                break;
            }
            if(curA->node == w) {
                ANode * next = curA->next;
                if(prev == NULL) {
                    kcas::add(&v->nHead, curA, next);
                }
                else {
                    kcas::add(&prev->next, curA, next);
                }
                break;
            }
            prev = curA;
            curA = curA->next;
        }
        if(retry) {
            continue;
        }

        prev = NULL;
        curA = w->nHead;
        while(true) {
            if(curA == NULL) {
                retry = true;
                break;
            }
            if(curA->node == v) {
                ANode * next = curA->next;
                if(prev == NULL) {
                    kcas::add(&w->nHead, curA, next);
                }
                else {
                    kcas::add(&prev->next, curA, next);
                }
                break;
            }
            prev = curA;
            curA = curA->next;
        }

        if(retry) {
            printf("%d cut 5\n", tid);
            continue;
        }
        if(kcas::execute()) return true;
    }
}

bool Forest::link(const int tid, GNode * v, GNode * w) {
    while(true) {
        bool retry = false;
        kcas::start();
        uint64_t vVer = v->ver;
        uint64_t wVer = w->ver;
        kcas::add(
                &v->ver, vVer, vVer + 1,
                &w->ver, wVer, wVer + 1);

        auto vRoot = getRoot(v);
        auto wRoot = getRoot(w);
        if(vRoot == wRoot) return false; //already linked
        if(wRoot != w) {
            uint64_t wRootVer = wRoot->ver;
            kcas::add(&wRoot->ver, wRootVer, wRootVer + 1); //synchro on root
        }
        if(vRoot != v) {
            uint64_t vRootVer = vRoot->ver;
            kcas::add(&vRoot->ver, vRootVer, vRootVer + 1); //synchro on root
        }

        LNode * A[MAX_TOWER_HEIGHT];
        LNode * B[MAX_TOWER_HEIGHT];
        LNode * C[MAX_TOWER_HEIGHT];
        LNode * D[MAX_TOWER_HEIGHT];
        LNode * E[MAX_TOWER_HEIGHT];
        LNode * F[MAX_TOWER_HEIGHT];
        LNode * G[MAX_TOWER_HEIGHT];
        LNode * H[MAX_TOWER_HEIGHT];
        LNode * VW[MAX_TOWER_HEIGHT];
        createTower(tid, v, w, VW);
        LNode * WV[MAX_TOWER_HEIGHT];
        createTower(tid, w, v, WV);

        LNode * vloop = v->loop;
        LNode * wloop = w->loop;
        LNode * vloopRight = vloop->right;
        LNode * wloopRight = wloop->right;

        LNode * vMin = getMin(v); //remove redundant call here
        LNode * vMinRight = vMin->right;

        LNode * wMin = getMin(w);
        LNode * wMinRight = wMin->right;

        LNode * vMax = getMax(v); //remove redundant call here
        LNode * vMaxLeft = vMax->left;

        LNode * wMax = getMax(w);
        LNode * wMaxLeft = wMax->left;

        LNode * S1[MAX_TOWER_HEIGHT];
        getSentinel(vMin, S1);
//        cout << "S1: ";
//        printTowers(S1);
        LNode * S4[MAX_TOWER_HEIGHT];
        getSentinel(wMax, S4);
//        cout << "S4: ";
//        printTowers(S4);

        getInternalTowers(vMinRight, vloop, A, B); //Get L1
//        cout << "A: ";
//        printTowers(A);
//        cout << "B: ";
//        printTowers(B);

        getInternalTowers(wMinRight, wloop, E, F); //Get L3
//        cout << "E: ";
//        printTowers(E);
//        cout << "F: ";
//        printTowers(F);

        if(vloopRight != vMax) { //L2 is not null
            getInternalTowers(vloopRight, vMaxLeft, C, D); //Get L2
        }
        else {
            nullTower(C);
            nullTower(D);
        }

//        cout << "C: ";
//        printTowers(C);
//        cout << "D: ";
//        printTowers(D);
//

        if(wloopRight != wMax) { //L4 is not null
//            cout << "wloopRight != wMAX" << endl;
            getInternalTowers(wloopRight, wMaxLeft, G, H); //Get L4
        }
        else {
            nullTower(G);
            nullTower(H);
        }
//        cout << "G: ";
//        printTowers(G);
//        cout << "H: ";
//        printTowers(H);

        for(int i = 0; i < MAX_TOWER_HEIGHT; i++) { //not sure if I'm proud or ashamed of this code
            LNode * old = S1[i]->right;
            kcas::add(&S1[i]->right, old, getFirstNonNull(i, C, A, VW, G, E, WV, S4));

            if(C[i] != NULL) {
                old = C[i]->left;
                kcas::add(&C[i]->left, old, S1[i]);
            }
            if(D[i] != NULL) {
                old = D[i]->right;
                kcas::add(&D[i]->right, old, getFirstNonNull(i, A, VW, G, E, WV, S4));
            }
            if(A[i] != NULL) {
                old = A[i]->left;
                kcas::add(&A[i]->left, old, getFirstNonNull(i, D, S1));
            }
            if(B[i] != NULL) {
                old = B[i]->right;
                kcas::add(&B[i]->right, old, getFirstNonNull(i, VW, G, E, WV, S4));
            }
            if(VW[i] != NULL) {
                VW[i]->left.setInitVal(getFirstNonNull(i, B, D, S1));
                VW[i]->right.setInitVal(getFirstNonNull(i, G, E, WV, S4));
            }
            if(G[i] != NULL) {
                old = G[i]->left;
                kcas::add(&G[i]->left, old, getFirstNonNull(i, VW, B, D, S1));
            }
            if(H[i] != NULL) {
                old = H[i]->right;
                kcas::add(&H[i]->right, old, getFirstNonNull(i, E, WV, S4));
            }
            if(E[i] != NULL) {
                old = E[i]->left;
                kcas::add(&E[i]->left, old, getFirstNonNull(i, H, VW, B, D, S1));
            }
            if(F[i] != NULL) {
                old = F[i]->right;
                kcas::add(&F[i]->right, old, getFirstNonNull(i, WV, S4));
            }
            if(WV[i] != NULL) {
                WV[i]->left.setInitVal(getFirstNonNull(i, F, H, VW, B, D, S1));
                WV[i]->right.setInitVal(S4[i]);
            }
            old = S4[i]->left;
            kcas::add(&S4[i]->left, old, getFirstNonNull(i, WV, F, H, VW, B, D, S1));
        }

        ANode * curA = v->nHead;
        ANode * newNode = new ANode(w, VW[0]);
        if(curA == NULL) {
            kcas::add(&v->nHead, (ANode *) NULL, newNode);
        }
        else {
            while(curA->next != NULL) {
                curA = curA->next;
                if(curA == NULL) {
                    retry = true;
                    break;
                }
            }
            if(retry) {
                continue;
            }
            kcas::add(&curA->next, (ANode *) NULL, newNode);
        }


        curA = w->nHead;
        newNode = new ANode(v, WV[0]);
        if(curA == NULL) {
            kcas::add(&w->nHead, (ANode *) NULL, newNode);
        }
        else {
            while(curA->next != NULL) {
                curA = curA->next;
                if(curA == NULL) {
                    retry = true;
                    break;
                }
            }
            if(retry) {
                continue;
            }
            kcas::add(&curA->next, (ANode *) NULL, newNode);
        }

        if(kcas::execute()) return true;

        continue;
    }

}

void Forest::getSentinel(LNode * node, LNode ** tower) {
    assert(node->from == NULL);
    for(int i = 0; i < MAX_TOWER_HEIGHT; i++) {
        tower[i] = node;
        node = node->up;
    }
}

void Forest::nullTower(LNode ** tower) {
    for(int i = 0; i < MAX_TOWER_HEIGHT; i++) {
        tower[i] = NULL;
    }
}

void Forest::getTower(LNode * start, LNode ** tower, bool forward) {
    int currIndex = 0;
    if(start != NULL) {
        bool done = false;
        while(!done) {
            tower[currIndex] = start;
            while(true) {
                if(start->up != NULL) {
                    start = start->up;
                    break;
                }
                else {
                    start = forward ? start->right : start->left;
                    if(start == NULL) {
                        done = true;
                        break;
                    }
                }
            }
            currIndex++;
        }
    }
    for(; currIndex < MAX_TOWER_HEIGHT; currIndex++) tower[currIndex] = NULL;
}

bool Forest::getInternalTowers(LNode * start, LNode * end, LNode ** left, LNode ** right) {
   // cout << "START! " << (start->from == NULL ? 0 : start->from->key) << "->" << (start->to == NULL ? 0 : start->to->key) << endl;
    //cout << "END! " << (end->from == NULL ? 0 : end->from->key) << "->" << (end->to == NULL ? 0 : end->to->key) << endl;
    int index = 0;
    bool foundSame = false;
    bool done = false;
    while((start != NULL || end != NULL) && index < MAX_TOWER_HEIGHT && !done) {
        left[index] = start;
        right[index] = end;
        bool sDone = false;
        bool eDone = false;
        while(!sDone || !eDone) {
            if(start == end) foundSame = true;

            if(start != NULL && !sDone) {
                if(start->up != NULL) {
                    start = start->up;
                    sDone = true;
                }
                else {
                    if(foundSame) {
                        done = true;
                        break;
                    }
                    else {
                        start = start->right;
                    }
                }
            }

            if(start == end) foundSame = true;

            if(end != NULL && !eDone) {
                if(end->up != NULL) {
                    end = end->up;
                    eDone = true;
                }
                else {
                    if(foundSame) {
                        done = true;
                        break;
                    }
                    else {
                        end = end->left;
                    }
                }
            }

            if(start == end) foundSame = true;
            if(start == NULL && end == NULL) return false;

        }

        index++;
    }

    for(; index < MAX_TOWER_HEIGHT; index++) {
        left[index] = NULL;
        right[index] = NULL;
    }
    return true;
}

bool Forest::connected(const int tid, GNode * u, GNode * v) {
    assert(u != NULL && v != NULL);
    while(true) {
        GNode * uRoot = getRoot(u);
        uint64_t uRootVer = uRoot->ver;
        GNode * vRoot = getRoot(v);
        uint64_t uRootVer2 = uRoot->ver;
        if(uRootVer == uRootVer2) {
            return uRoot == vRoot;
        }
    }
}

LNode * Forest::getFirstNonNull(int height, LNode ** tower) {
    assert(tower[height] != NULL);
    return tower[height];
}

template<typename... Args>
LNode * Forest::getFirstNonNull(int height, LNode ** tower, Args... args) {
    if(tower[height] != NULL) return tower[height];
    else return getFirstNonNull(height, args...);
}

//concurrent

GNode * Forest::getRoot(GNode * node) {
    while(true) {
        bool retry = false;
        LNode * currNode = node->loop;

        while(currNode->left != NULL) {
            if(currNode->up != NULL) currNode = currNode->up;
            else currNode = currNode->left;

            if(currNode == NULL) {
                retry = true;
                break;
            }
        }

        if(retry) continue;

        while(currNode->down != NULL) {
            currNode = currNode->down;
            if(currNode == NULL) {
                retry = true;
                break;
            }
        }

        if(retry) continue;

        currNode = currNode->right;
        if(currNode && currNode->from != NULL) return currNode->from;
    }
}


GNode * Forest::getRoot(LNode * node) {
    while(true) {
        bool retry = false;
        LNode * currNode = node;

        while(currNode->left != NULL) {
            if(currNode->up != NULL) currNode = currNode->up;
            else currNode = currNode->left;

            if(currNode == NULL) {
                retry = true;
                break;
            }
        }

        if(retry) continue;

        while(currNode->down != NULL) {
            currNode = currNode->down;
            if(currNode == NULL) {
                retry = true;
                break;
            }
        }

        if(retry) continue;

        currNode = currNode->right;
        if(currNode && currNode->from != NULL) return currNode->from;
    }
}

LNode * Forest::getMin(GNode * node) {
    LNode * prev = NULL;
    LNode * curr = node->loop;
    while(curr != NULL) {
        prev = curr;
        curr = curr->left;
    }
    return prev;
}

LNode * Forest::getMax(GNode * node) {
    LNode * prev = NULL;
    LNode * curr = node->loop;
    while(curr != NULL) {
        prev = curr;
        curr = curr->right;
    }
    return prev;
}

bool Forest::areNeighbours(GNode * u, GNode * v) {
    ANode * currN = u->nHead;
    while(currN != NULL) {
        if(currN->node == v) return true;
        currN = currN->next;
    }
    return false;
}

void Forest::printDebugging(GNode * gNode) {

}

void Forest::graph(GNode * node, int arg, int arg2) {
    ofstream dot;
    std::unordered_set<GNode *> gNodes = {};
    std::unordered_set<LNode *> lNodes = {};
    dot.open("graphs/graph" + std::to_string(arg) + "-" + std::to_string(arg2) + ".dot");

    dot << " strict digraph master{\n";
    dot << "rankdir = BT" << endl;


    dot << " subgraph L {\n";
    dot << "rank = same" << endl;
    auto a = getMin(node);
    outputList(getMin(node), dot, 0, 0, lNodes);
    dot << "}\n";

    dot << " subgraph G {\n";

    outputGraph(getRoot(node), dot, 0, -1, gNodes);
    dot << "}\n";
    dot << "}\n";

    dot.close();

}

bool Forest::outputGraph(GNode * gNode, ofstream &dot, int right, int down, std::unordered_set<GNode *> &gNodes) {
    if(gNodes.count(gNode)) return false;
    gNodes.insert(gNode);

    dot << "\"" << gNode << "\"" << "[label=\"G: " << gNode->key << "\" pos=\"" << right * 2 << ", " << down * 2 << "!\"];\n";
    ANode * currA = gNode->nHead;
    int numChildren = 0;
    dot << "\"" << gNode << "\" -> \"" << gNode->loop << "\" [color=\"red\"]\n";
    if(currA == NULL) return true;
    else {
        while(currA != NULL) {
            dot << "\"" << gNode << "\" -> \"" << currA->node << "\"\n";
            bool res = outputGraph(currA->node, dot, right + 1, down - numChildren, gNodes);

            currA = currA->next;
            if(res) numChildren++;
        }
    }

    return true;
}

void Forest::outputList(LNode * lNode, ofstream &dot, int up, int right, std::unordered_set<LNode *> &lNodes) {
    if(lNode == NULL || lNodes.count(lNode)) return;
    lNodes.insert(lNode);
    dot << "\"" << lNode << "\"" << "[label=\"" << (lNode->from == NULL ? -1 : lNode->from->key) << "->" << (lNode->to == NULL ? -1 : lNode->to->key) << "\" pos=\"" << right << "," << up << "!\"]\n";
    if(lNode->right != NULL) {
        outputList(lNode->right, dot, up, right + 1, lNodes);
        dot << "\"" << lNode << "\" -> \"" << lNode->right << "\"\n";
        assert(lNode->right->left == lNode);
    }
    if(lNode->left != NULL) {
        dot << "\"" << lNode << "\" -> \"" << lNode->left << "\"\n";
        assert(lNode->left->right == lNode);
    }

    if(lNode->up != NULL) {
        outputList(lNode->up, dot, up + 1, right, lNodes);
        dot << "\"" << lNode << "\" -> \"" << lNode->up << "\" \n";
    }

    if(lNode->down != NULL) {
        dot << "\"" << lNode << "\" -> \"" << lNode->down << "\" \n";
    }
}

void Forest::printTowers(LNode ** t) {
    cout << "Tower: ";
    for(int i = 0; i < MAX_TOWER_HEIGHT; i++) {
        if(t[i] == NULL) {
            cout << " H: " << i << " NULL  ";
        }
        else {
            cout << " H: " << i << " G: " << (t[i]->from == NULL ? -1 : t[i]->from->key) << " -> " << (t[i]->to == NULL ? -1 : t[i]->to->key) << "   ";
        }
    }
    cout << endl << endl;
}
