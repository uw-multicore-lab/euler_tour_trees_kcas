#include <thread>
#include <cstdlib>
#include <atomic>
#include <string>
#include <cstring>
#include <iostream>

#include "util.h"

#include "forest.h"
#define MAX_NODES 1000000
#include "defines.h"
#include "util.h"

using namespace std;

struct globals_t {
    PaddedRandom rngs[MAX_THREADS];
    volatile char padding0[PADDING_BYTES];
    ElapsedTimer timer;
    volatile char padding1[PADDING_BYTES];
    ElapsedTimer timerFromStart;
    volatile char padding3[PADDING_BYTES];
    volatile bool done;
    volatile char padding4[PADDING_BYTES];
    volatile bool start; // used for a custom barrier implementation (should threads start yet?)
    volatile char padding5[PADDING_BYTES];
    atomic_int running; // used for a custom barrier implementation (how many threads are waiting?)
    volatile char padding6[PADDING_BYTES];
    Forest * ds;
    debugCounter numTotalOps; // already has padding built in at the beginning and end
    debugCounter linkSum;
    debugCounter sizeChecksum;
    int millisToRun;
    int totalThreads;
    int numNodes;
    volatile char padding7[PADDING_BYTES];
    size_t garbage; // garbage variable that will be useful for preventing some code from being optimized out
    volatile char padding8[PADDING_BYTES];
    GNode * nodes[MAX_NODES];

    globals_t(int _millisToRun, int _totalThreads, int _numNodes, Forest * forest) {

        for(int i = 0; i < MAX_THREADS; ++i) {
            rngs[i].setSeed(i + 1); // +1 because we don't want thread 0 to get a seed of 0, since seeds of 0 usually mean all random numbers are zero...
        }

        done = false;
        start = false;
        running = 0;
        millisToRun = _millisToRun;
        totalThreads = _totalThreads;
        numNodes = _numNodes;
        garbage = -1;
        ds = forest;

        for(int i = 0; i < numNodes; i++) {
            nodes[i] = new GNode(i + 1);
            ds->addRoot(nodes[i]);
        }
    }

    ~globals_t() {
        delete ds;
    }
} __attribute__((aligned(PADDING_BYTES)));

void runTrial(globals_t * g, const long millisToRun, double linkPercent, double cutPercent) {
    g->done = false;
    g->start = false;

    // create and start threads
    thread * threads[MAX_THREADS]; // just allocate an array for max threads to avoid changing data layout (which can affect results) when varying thread count. the small amount of wasted space is not a big deal.
    for(int tid = 0; tid < g->totalThreads; ++tid) {
        threads[tid] = new thread([&, tid]() { /* access all variables by reference, except tid, which we copy (since we don't want our tid to be a reference to the changing loop variable) */
            const int OPS_BETWEEN_TIME_CHECKS = 500; // only check the current time (to see if we should stop) once every X operations, to amortize the overhead of time checking
            size_t garbage = 0; // will prevent contains() calls from being optimized out

            // BARRIER WAIT
            g->running.fetch_add(1);
            while(!g->start) {
                                  TRACE TPRINT("waiting to start" << endl);
            } // wait to start

            int key = 0;
            for(int cnt = 0; !g->done; ++cnt) {
                if((cnt % OPS_BETWEEN_TIME_CHECKS) == 0 && g->timer.getElapsedMillis() >= millisToRun) g->done = true; // set global "done" bit flag, so all threads know to stop on the next operation (first guy to stop dictates when everyone else stops --- at most one more operation is performed per thread!)

                // flip a coin to decide: insert or erase?
                // generate a random double in [0, 100]
                double operationType = g->rngs[tid].nextNatural() / (double) numeric_limits<unsigned int>::max() * 100;

                GNode * a = g->nodes[g->rngs[tid].nextNatural() % g->numNodes];
                GNode * b = g->nodes[g->rngs[tid].nextNatural() % g->numNodes];
                cout << a->key << endl;
                g->ds->graph(a, 0, 0);
                cout << b->key << endl;
                g->ds->graph(b, 1, 0);

                if(operationType < linkPercent) {
                auto result = g->ds->link(tid, a, b);
                    if(result) {
                        g->linkSum.add(tid, 2);
                        g->sizeChecksum.add(tid, 1);
                    }
                }
                else if(operationType < linkPercent + cutPercent) {
                    auto result = g->ds->cut(tid, a, b);
                    if(result) {
                        g->linkSum.add(tid, -2);
                    }
                }
                else {
                    auto result = g->ds->connected(tid, a, b);
                    garbage += result; // "use" the return value of contains, so contains isn't optimized out
                }

                    g->numTotalOps.inc(tid);
                }

            g->running.fetch_add(-1);
            __sync_fetch_and_add(&g->garbage, garbage); // "use" the return values of all contains
        });
    }

    while(g->running < g->totalThreads) {
        TRACE cout << "main thread: waiting for threads to START running=" << g->running << endl;
    }
    g->timer.startTimer();
    __sync_synchronize(); // prevent compiler from reordering "start = true;" before the timer start; this is mostly paranoia, since start is volatile, and nothing should be reordered around volatile reads/writes
    g->start = true; // release all threads from the barrier, so they can work

    while(g->running > 0) {
        /* wait for all threads to stop working */
    }


    // join all threads
    for(int tid = 0; tid < g->totalThreads; ++tid) {
        threads[tid]->join();
        delete threads[tid];
    }
}

void runExperiment(int numNodes, int millisToRun, int totalThreads, double insertPercent, double deletePercent) {
    // create globals struct that all threads will access (with padding to prevent false sharing on control logic meta data)

    auto dataStructure = new Forest(totalThreads);
    auto g = new globals_t(millisToRun, totalThreads, numNodes, dataStructure);

    /**
     * 
     * RUN EXPERIMENT
     * 
     */

    cout << "main thread: experiment starting..." << endl;
    runTrial(g, g->millisToRun, insertPercent, deletePercent);
    cout << "main thread: experiment finished..." << endl;
    cout << endl;

    /**
     * 
     * PRODUCE OUTPUT
     * 
     */


    auto numTotalOps = g->numTotalOps.getTotal();
    auto nodesNumLinks = 0;
    int selfLinks = 0;
    std::unordered_set<LNode *> lNodes = {};
    std::unordered_set<LNode *> mins = {};

    for(int i = 0; i < g->numNodes; i++) {
        LNode * node = g->ds->getMin(g->nodes[i]);
        if(!mins.count(node)) {
            while(node != NULL) {
                if(node->from == node->to) {
                    selfLinks++;
                }
                if(lNodes.count(node)) {
                    cout << "Duplicate link found " << endl;
                    //assert(false);
                }
                lNodes.insert(node);
                nodesNumLinks++;
                node = node->right;
            }
            mins.insert(node);
        }
    }

    auto threadTotal = g->linkSum.getTotal();

    cout << "Validation: num links according to nodes = " << nodesNumLinks << " and sum of keys according to the threads = " << threadTotal << ".";
    cout << ((threadTotal == nodesNumLinks) ? " OK." : " FAILED.") << endl;
    cout << "sizeChecksum=" << g->sizeChecksum.getTotal() << endl;
    cout << endl;

    cout << "completedOperations=" << numTotalOps << endl;
    cout << "throughput=" << (long long) (numTotalOps * 1000. / g->millisToRun) << endl;
    cout << endl;

    if(threadTotal != nodesNumLinks) {
        cout << "ERROR: validation failed!" << endl;
        exit(0);
    }

    if(g->garbage == 0) cout << endl; // "use" the g->garbage variable, so effectively the return values of all contains() are "used," so they can't be optimized out
    cout << "total elapsed time=" << (g->timerFromStart.getElapsedMillis() / 1000.) << "s" << endl;
    delete g;
}

int main(int argc, char** argv) {
    if(argc == 1) {
        cout << "USAGE: " << argv[0] << " [options]" << endl;
        cout << "Options:" << endl;
        cout << "    -t [int]     milliseconds to run" << endl;
        cout << "    -s [int]     total number of graph nodes" << endl;
        cout << "    -n [int]     number of threads that will perform inserts and deletes" << endl;
        cout << "    -l [double]  percent of operations that will be insert (example: 20)" << endl;
        cout << "    -c [double]  percent of operations that will be delete (example: 20)" << endl;
        cout << "                 (100 - i - d)% of operations will be contains" << endl;
        cout << endl;
        return 1;
    }

    int millisToRun = -1;
    int numNodes = 0;
    int totalThreads = 0;
    double linkPercent = 0;
    double cutPercent = 0;
    bool reclaim = false;

    // read command line args
    for(int i = 1; i < argc; ++i) {
        if(strcmp(argv[i], "-s") == 0) {
            numNodes = atoi(argv[++i]);
        }
        else if(strcmp(argv[i], "-n") == 0) {
            totalThreads = atoi(argv[++i]);
        }
        else if(strcmp(argv[i], "-t") == 0) {
            millisToRun = atoi(argv[++i]);
        }
        else if(strcmp(argv[i], "-l") == 0) {
            linkPercent = atof(argv[++i]);
        }
        else if(strcmp(argv[i], "-c") == 0) {
            cutPercent = atof(argv[++i]);
        }
        else {
            cout << "bad arguments" << endl;
            exit(1);
        }
    }

    // print command and args for debugging
    std::cout << "Cmd:";
    for(int i = 0; i < argc; ++i) {
        std::cout << " " << argv[i];
    }
    std::cout << std::endl;

    // print configuration for debugging
    PRINT(MAX_THREADS);
    PRINT(totalThreads);
    PRINT(numNodes);
    PRINT(linkPercent);
    PRINT(cutPercent);
    PRINT(millisToRun);
    cout << endl;

    // check for too large thread count
    if(totalThreads >= MAX_THREADS) {
        std::cout << "ERROR: totalThreads=" << totalThreads << " >= MAX_THREADS=" << MAX_THREADS << std::endl;
        return 1;
    }


    runExperiment(numNodes, millisToRun, totalThreads, linkPercent, cutPercent);

    return 0;
}