GPP = g++
FLAGS = -O3 -g
#FLAGS += -DNDEBUG
LDFLAGS = -pthread

PROGRAMS = main

all: $(PROGRAMS)

build:
	mkdir -p $@ graphs
	cp -f libjemalloc.so $@/.

main: build
	$(GPP) $(FLAGS) -MMD -MP -MF build/$@.d -o $@ $@.cpp $(LDFLAGS)
	
-include $(addprefix build/,$(addsuffix .d, $(PROGRAMS)))

clean:
	rm -rf $(PROGRAMS) build graphs/*



